package PC;

public class OurPC {

	public OurPC(Case model, Monitor size, Motherboard ramSlots) {

	}

	public static void main(String[] args) {

		Dimensions dimensions = new Dimensions(20, 20, 5);

		Case theCase = new Case("220B", "Dell", "240", "dimensions");

		Monitor theMonitor = new Monitor("27inch beast", "Acer", 27, new Resolution(2560, 1440));

		Motherboard theMotherboard = new Motherboard("BJ-200", "Asus", 4, 6, "v2.44");

		OurPC thePC = new OurPC(theCase, theMonitor, theMotherboard);
		thePC.getTheCase().pressPowerButton();

	}

	private Case getTheCase() {
		Case theCase = new Case("220B", "Dell", "240", "dimensions");
		return theCase;
	}

}
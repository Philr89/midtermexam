package BankQuestion5;

public abstract class Bank {

	public abstract double getBalance();

	public static void main(String[] args) {

		BankA a = new BankA();
		BankB b = new BankB();
		BankC c = new BankC();

		System.out.println("The balance of bank A is " + a.getBalance());
		System.out.println("The balance of bank B is " + b.getBalance());
		System.out.println("The balance of bank C is " + c.getBalance());
	}

}
